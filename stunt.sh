#!/usr/bin/env bash
#    Copyright 2018 gusto
# 
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
# 
#        http://www.apache.org/licenses/LICENSE-2.0
# 
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

. "$(dirname $0)/function.d/init"

RUN_USER=$(logname)
eval USER_HOME="~$RUN_USER"

PRIVATE_KEY_FILE=""
SSH_OPTS="TCPKeepAlive=yes ServerAliveInterval=20 ServerAliveCountMax=3"
TUNNEL_OPTS=""
EXCL_HOSTS=""

showHelp () {
    echo "Usage: $0 [options] [month ...]" >&2
    echo "" >&2
    echo "Options:" >&2
    echo " -h : show this help" >&2
    echo " -i : private key file" >&2
    echo " -r : remote host" >&2
    echo " -t <file> : target host list" >&2
    echo " -P : SSH agent PID, usually \$SSH_AGENT_PID variable" >&2
    echo " -S : SSH agent socket, usually \$SSH_AUTH_SOCK variable" >&2
    echo " -A : SSH agent password prompt, usually \$SSH_ASKPASS variable" >&2
    echo " -x : exclude host/subnet" >&2
    
    echo "" >&2
}

IS_DEBUG="FALSE"

REMOTE_HOST=""
TARGET_HOST_FILE=""
AGENT_PID=""
AGENT_PASS=""
AGENT_SOCK=""
INCL_HOSTS=""
while getopts "dDhH:i:o:r:t:x:P:A:S:" opt; do
    case $opt in
        d)
            IS_DEBUG="TRUE"
            ;;
        D)
            TUNNEL_OPTS="$TUNNEL_OPTS --dns"
            ;;
        h)
            showHelp
            exit 0
            ;;
        H)
            INCL_HOSTS="$INCL_HOSTS $OPTARG"
            ;;
        i)
            PRIVATE_KEY_FILE="$OPTARG"
            ;;
        o)
            SSH_OPTS="$SSH_OPTS $OPTARG"
            ;;
        r)
            REMOTE_HOST="$OPTARG"
            ;;
        t)
            TARGET_HOST_FILE="$OPTARG"
            ;;
        x)
            EXCL_HOSTS="$EXCL_HOSTS $OPTARG"
            ;;
        P)
            AGENT_PID="$OPTARG"
            ;;
        A)
            AGENT_PASS="$OPTARG"
            ;;
        S)
            AGENT_SOCK="$OPTARG"
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            showHelp
            exit 1
            ;;
    esac
done

if [ "$TARGET_HOST_FILE" != "" ]; then
    for a in $(cat "$TARGET_HOST_FILE"); do
        INCL_HOSTS="$INCL_HOSTS $a"
    done
fi
INCL_HOSTS=$(echo $INCL_HOSTS)

if [ "$INCL_HOSTS" == "" ]; then
    INCL_HOSTS="0.0.0.0/0"
fi

EXCL_HOSTS=$(echo $EXCL_HOSTS)

CMD_SSH="ssh"
if [ "$PRIVATE_KEY_FILE" != "" ]; then
    CMD_SSH="$CMD_SSH -i $PRIVATE_KEY_FILE"
fi
for a in $SSH_OPTS; do
    CMD_SSH="$CMD_SSH -o $a"
done

CMD_BASE="sshuttle"
for a in $TUNNEL_OPTS; do
    CMD_BASE="$CMD_BASE $a"
done
CMD_BASE="$CMD_BASE -v"
CMD_BASE="$CMD_BASE -r $REMOTE_HOST"
for a in $EXCL_HOSTS; do
    CMD_BASE="$CMD_BASE -x $a"
done

if [ "$IS_DEBUG" == "TRUE" ]; then
    echo "Run from: $(logname)"
    echo "SSH_AGENT_PID=\"$AGENT_PID\" SSH_ASKPASS=\"$AGENT_PASS\" SSH_AUTH_SOCK=\"$AGENT_SOCK\" $CMD_BASE -e \"$CMD_SSH\" $INCL_HOSTS"
else
    SSH_AGENT_PID="$AGENT_PID" SSH_ASKPASS="$AGENT_PASS" SSH_AUTH_SOCK="$AGENT_SOCK" $CMD_BASE -e "$CMD_SSH" $INCL_HOSTS
fi
